import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

import EditorRouter from '@/views/Editor/MainRouter'
import EditorMain from '@/views/Editor/Main'
import EditorQuestionAdding from '@/views/Editor/EditorQuestionAdding'
import ToDoMenu from '@/views/Editor/ToDo/ToDoMenu';
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    { 
      path: '/editor', 
      component: EditorRouter,
      children: [
        {
          path: '',
          name: 'EditorMain',
          component: EditorMain
        },
        {
          path: 'add-question',
          name: 'EditorQuestionAdding',
          component: EditorQuestionAdding
        },
       {
         path:'to-do',
         name:'ToDo',
         component:ToDoMenu
       } 
      ]
    }
  ]
})